﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MusicMaster : MonoBehaviour {

	public AudioMixer mastermixer;
	public float startMusicVol;
	public float startSFXVol;
	public float startMasterVol;
	public Slider MasterVolSlider;
	public Slider MusicVolSlider;
	public Slider SfxVolSlider;

	void Start(){
		mastermixer.SetFloat("MasterVol",startMasterVol);
		MasterVolSlider.value=startMasterVol;
		mastermixer.SetFloat("MusicVol",startMusicVol);
		MusicVolSlider.value=startMusicVol;
		mastermixer.SetFloat("sfxVol",startSFXVol);
		SfxVolSlider.value = startSFXVol;
	}

	public void ChangeMasterVol(float volume){
		mastermixer.SetFloat("MasterVol",volume);
	}

	public void ChangeMusicVol(float volume){
		mastermixer.SetFloat("MusicVol",volume);
	}

	public void ChangeSFXVol(float volume){
		mastermixer.SetFloat("sfxVol",volume);
	}

}
