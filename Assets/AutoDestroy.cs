﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {

	public float Delay;

	float TimeOfDestroy;
	// Use this for initialization

	void Start () {
		TimeOfDestroy = Time.time + Delay;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time >= TimeOfDestroy){
			Destroy(gameObject);
		}
	}
}
