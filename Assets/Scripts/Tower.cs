﻿using UnityEngine;
using System.Collections;

public enum TowerType{Diamond,Sapphire,Topaz,Emerald,Aquamarine,Opal,Ruby,Amethyst,Rock,Tower};
public enum TowerSize{Chipped,Flawed,Normal,Flawless,Perfect,Great,Maze,Special}

public class Tower : MonoBehaviour {	
	public TowerType myType;
	public TowerSize mySize;
	
	public GameObject HoverObj;
	public GameObject SelectedObj;
	public GameObject myTurret;
	public bool Sellable;
	
	public bool CanBuildOnMe;
	[Header("SlateTower")]
	public bool isSlate;

	[Header("SpecialTower")]
	public bool isSpecial;
	public string SpecialName;
	public GameObject[] Ingredients;
	public GameObject SpecialUpgrade;
	public int UpgradeCost;
	[Header("NonSpecial")]
	public bool HasSlate;
	public GameObject mySlate;
	public bool HasSpecial;
	public GameObject mySpecial;
	public GameObject DowngradeObj;
	public GameObject CombineObj;

	[HideInInspector]
	public bool MouseHover;
	[HideInInspector]
	public bool Selected;
	[HideInInspector]
	public bool TempOrNot;
	[HideInInspector]
	public bool SlateMoved;


	void Start(){
		SlateMoved = false;
		if(SelectedObj && HoverObj){
			HoverObj.SetActive(false);
			SelectedObj.SetActive(false);
			TempOrNot = true;
		}
	}
	
	void Update(){
		if(GameMaster.GameMode == 3){
			TempOrNot = false;
		}
	}
	public void Select(bool io){
		if(SelectedObj && HoverObj){
			if(io == true){			
				SelectedObj.SetActive(true);
				HoverObj.SetActive(false);
				Selected = true;			
			}else{
				SelectedObj.SetActive(false);
				Selected = false;
			}
		}
	}
	
	public void Hover(bool io){
		if(SelectedObj && HoverObj){
			if(io == true){
			if(Selected == false){
					HoverObj.SetActive(true);
				}
				MouseHover = true;
			}else{			
					HoverObj.SetActive(false);
					MouseHover = false;
			}
		}
	}
	
}
