﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class Turret : MonoBehaviour {
		
	[Header("Basic Options")]	
	public int MinDmg;
	public int MaxDmg;
	public float AttackCooldown;	
	public float AttackRange;	
	public bool LookAtTarget;	
	public int MaxTargets; // 0 = no max
	public bool GroundOnly;
	public bool AirOnly;
	public GameObject AttackLinePrefab;
	public GameObject MySpeaker;
	public AudioClip MyClip;
	[Header("ChanceToMultiplyDMG")]
	public bool CanCrit;
	public int percentChancetoMultiply;
	public int MultiplyDMGBy;
	[Header("Splash Dmg Options")]
	public bool SplashTower;
	public float SplashSize;
	public int SplashDmgToOthers;
	[Header("Trap Options")]
	public bool TrapTower;
	public float TrapDuration;
	[Header("Slow Options")]
	public bool SlowTower;
	public int PercentChancetoSlow;
	public float SlowDuration;
	public int SlowPercent;
	[Header("Poison Dmg Options")]
	public bool PoisonTower;
	public int PoisonDmg;
	public float PoisonCooldown;
	public float PoisonDuration;
	[Header("SplashEffect")]
	public bool SplashSlow;
	public float SplashSlowPercentChance;
	public float SplashSlowRange;
	public int SplashSlowPercentSlow;
	public float SplashSlowDuration;
	[Header("AttackSpeedBuff Options")]
	public bool BuffsOtherTowers;
	public int percentToReduceCD;
	public int BuffRange;
	[Header("Other")]
	public int percentCDReduction;
	
	List<GameObject> TargetList;

	bool AttackedThisFrame;
	float TimeOfNextAttack;
	float TimeOfNextReTarget;
	AudioSource myAuidoSource;
	
	void Start(){
		myAuidoSource = MySpeaker.GetComponent<AudioSource>();
		Tower parentTower = GetComponentInParent<Tower>();
		switch(parentTower.mySize.ToString()){
		case "Chipped" : 
			myAuidoSource.pitch += .2f;
			myAuidoSource.volume -= .4f;
			break;
		case "Flawed" :
			myAuidoSource.pitch += .1f;
			myAuidoSource.volume -= .2f;
			break;
		case "Normal" :
			break;
		case "Flawless" :
			myAuidoSource.pitch -= .1f;
			myAuidoSource.volume += .2f;
				break;
		case "Perfect" :
			myAuidoSource.pitch -= .2f;
			myAuidoSource.volume += .3f;
			break;
		case "Great" :
			myAuidoSource.pitch -= .3f;
			myAuidoSource.volume += .4f;
			break;
		default :
			myAuidoSource.pitch -= .4f;
			break;
		};
		TargetList = new List<GameObject>();
		TimeOfNextAttack = Time.time;
		TimeOfNextReTarget = Time.time;
		TargetList = new List<GameObject>();
		myAuidoSource.clip = MyClip;
	}
	
	// Update is called once per frame
	void Update () {
		AttackedThisFrame = false;
		if(GameMaster.GameMode == 3){
		
			if(Time.time >= TimeOfNextReTarget){
				TargetList = new List<GameObject>();
				if(GameMaster.CurrentCreeps.Count > 0){
					for(int i = 0; i < GameMaster.CurrentCreeps.Count; i++){
						if(Vector3.Distance(GameMaster.CurrentCreeps[i].transform.position,this.gameObject.transform.position) <= AttackRange){
							Creep tempCreepS = GameMaster.CurrentCreeps[i].GetComponent<Creep>();
							bool isTargetable = true;
							if(tempCreepS.Air && GroundOnly){
								isTargetable = false;
							}
							if(tempCreepS.Ground && AirOnly){
								isTargetable = false;
							}
							if(isTargetable == true){
								TargetList.Add(GameMaster.CurrentCreeps[i]);
							}
						}
					}

				TimeOfNextReTarget = AttackCooldown * .5f;
			}

			if(Time.time >= TimeOfNextAttack){
				//create new list of all targets in range.					
					if(TargetList.Count > 0){
						//this tower can attack all creeps in list.					
						if(MaxTargets == 0){
							for(int i = 0; i < TargetList.Count; i++){
								Creep TempCreep = TargetList[i].GetComponent<Creep>();						
								Attack(TempCreep);
								AttackedThisFrame = true;
							}
						//this tower has a set number of targets it can shoot.
						}else{
							for(int i = 0; i < MaxTargets; i++){
								if(i < TargetList.Count){
									Creep TempCreep = TargetList[i].GetComponent<Creep>();
									Attack (TempCreep);
									AttackedThisFrame = true;
								}
							}
						}
					}

				}
				if(AttackedThisFrame == true){
					if(MyClip){
						myAuidoSource.Play();
					}
						//OPAL CHECK HERE!!!
						percentCDReduction = 0;
						for(int Z = 0; Z < 60; Z++){
							for(int X = 0; X < 60; X++){
								if(GameMaster.TowersArray[Z,X] != null){
								GameObject thisTower = GameMaster.TowersArray[Z,X];
									if(thisTower.tag == "Tower"){
										Tower temptower = thisTower.GetComponent<Tower>();
										Turret tempturret = temptower.myTurret.GetComponent<Turret>();
										if(tempturret.BuffsOtherTowers == true && Vector3.Distance(temptower.transform.position,this.transform.position) <= tempturret.BuffRange){
											if(tempturret.percentToReduceCD > percentCDReduction){
												percentCDReduction = tempturret.percentToReduceCD;
											}
										}
									}
								}
							}
						}
						
						

						//reset attack cooldown			
						float newcooldown = (AttackCooldown - ((percentCDReduction / 100) * AttackCooldown));
						TimeOfNextAttack = Time.time + newcooldown;
				}
			}
		}
	}//end of Update
	
	
	//attacks one creep, no checks are done here.
	void Attack(Creep Victem){
		if(Victem){
			GameObject TempAttackLine = Instantiate(AttackLinePrefab,this.gameObject.transform.position,Quaternion.identity) as GameObject;
			LineRenderer TempLine = TempAttackLine.GetComponent<LineRenderer>();
			TempLine.SetPosition(0, Vector3.zero);
			TempLine.SetPosition(1, Victem.transform.position - this.gameObject.transform.position);
			if(PoisonTower){
				Victem.Poison(PoisonDmg,PoisonDuration,PoisonCooldown);
			}
			
			if(CanCrit){
				int rand = Random.Range(0,100);
				if(rand <= percentChancetoMultiply){
					int tempdmg = (Random.Range(MinDmg,MaxDmg)) * MultiplyDMGBy;
					Victem.TakeDmg(tempdmg);
				}else{
					Victem.TakeDmg(Random.Range(MinDmg,MaxDmg));
				}
			}else{
				Victem.TakeDmg(Random.Range(MinDmg,MaxDmg));
			}
			
			if (SplashTower) {
				if(GameMaster.CurrentCreeps.Count > 1){
					for(int i = 0; i < GameMaster.CurrentCreeps.Count; i++){
						if(Vector3.Distance(GameMaster.CurrentCreeps[i].transform.position,Victem.transform.position) < SplashSize){
							Creep tempcreep = GameMaster.CurrentCreeps[i].GetComponent<Creep>();
							tempcreep.TakeDmg(SplashDmgToOthers);
							Vector3 newdir = Victem.transform.position - GameMaster.CurrentCreeps[i].transform.position;
							Debug.DrawRay(GameMaster.CurrentCreeps[i].transform.position + (Vector3.up*.25f), newdir + (Vector3.up*.25f), Color.red,.2f);
						}
					}
				}
			}
			if (TrapTower) {
				Victem.Trap(TrapDuration);
			}
			
			if(SlowTower){
				int rand = Random.Range(0,100);
				if(rand <= PercentChancetoSlow){
					Victem.Slow(SlowPercent,SlowDuration);
				}				
			}
			
			if (SplashSlow) {
			int rand = Random.Range(0,100);
				if(rand <= SplashSlowPercentChance){
					for(int i = 0; i < GameMaster.CurrentCreeps.Count; i++){
						if(Vector3.Distance(GameMaster.CurrentCreeps[i].transform.position,Victem.transform.position) < SplashSlowRange){
							Creep tempcreep = GameMaster.CurrentCreeps[i].GetComponent<Creep>();
							tempcreep.Slow (SplashSlowPercentSlow,SplashSlowDuration);
							Vector3 newdir = Victem.transform.position - GameMaster.CurrentCreeps[i].transform.position;
							Debug.DrawRay(GameMaster.CurrentCreeps[i].transform.position + (Vector3.up*.25f), newdir + (Vector3.up*.25f), Color.red,.2f);
						}
					}
				}
			}
			
			
			Vector3 tempdir = Victem.transform.position - this.gameObject.transform.position;
			Debug.DrawRay(this.gameObject.transform.position,tempdir+(Vector3.up*.25f),Color.green,.2f);
		}
	}
	
	
	
}
