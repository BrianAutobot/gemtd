﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour {

	[Header("CreepWave Settings")]
		public GameObject[] GroundCreeps;
		public GameObject[] AirCreeps;
		[Range(1,50)]
		public int CreepsPerWave;
		[Range(.25f,5.0f)]
		public float SecondsBetweenCreeps;
		public int startingCreepHP;
		public double CreepHpMultiplier;
		public int GoldPerCreepper4levels;
		public GameObject[] Waypoints;
		public int StartingLives;
		public int[] Odds0;
		public int[] Odds1;
		public int[] Odds2;
		public int[] Odds3;
		public int[] Odds4;
		public int[] Odds5;
		public int[] Odds6;
		public int[] Odds7;
		public int[] Odds8;
		public int[] Odds9;
		public int[] ChanceUpgradeCosts;
	
	[Header("UI")]
				//chances % display
				public Text CurrentChipped;
				public Text CurrentFlawed;
				public Text CurrentNormal;
				public Text CurrentFlawless;
				public Text CurrentPerfect;
				public Text NextChipped;
				public Text NextFlawed;
				public Text NextNormal;
				public Text NextFlawless;
				public Text NextPerfect;
		//UI Buttons and stuff
			public GameObject twoXTimeButton;
			public GameObject oneXTimeButton;
			
			public Text WaveCountText;
			public Text HpPerCreepText;
			public Text CurrentGoldText;
			public Text CurrentLivesText;
			public Text CurrentKillsText;
			public Text UpgradeCostText;
		public GameObject OptionsPanel;
		public GameObject HelpPanel;
		public GameObject TowerMenu;
			public GameObject SelectButton;
			public GameObject SellButton;
			public GameObject SpecialButton;
			public GameObject SlateButton;
		public GameObject MoveSlateButton;
			public GameObject CombineButton;
			public GameObject DowngradeButton;
			public GameObject UpgradeButton;
			
			public Text TowerName;
			public Text TowerDmg;
			public Text TowerCooldown;
			public Text TowerRange;
			public Text mySpecialName;
			public Text mySpecialParts;
			public GameObject BuildButton;
			public GameObject ReplaceBtn;
			public GameObject UpgradeChancesButton;			
			
			public Text CostToUpgradeChancesText;
	
	[Header("Tower Prefabs")]		
		public GameObject MazeRock;		
		public GameObject TowerPlacer;
		
		public GameObject ChippedDiamond;
		public GameObject ChippedSaphire;
		public GameObject ChippedTopaz;
		public GameObject ChippedAqua;
		public GameObject ChippedEmerald;
		public GameObject ChippedRuby;
		public GameObject ChippedAmethyst;
		public GameObject ChippedOpal;
		//
		public GameObject FlawedDiamond;
		public GameObject FlawedSaphire;
		public GameObject FlawedTopaz;
		public GameObject FlawedAqua;
		public GameObject FlawedEmerald;
		public GameObject FlawedRuby;
		public GameObject FlawedAmethyst;
		public GameObject FlawedOpal;
		//
		public GameObject NormalDiamond;
		public GameObject NormalSaphire;
		public GameObject NormalTopaz;
		public GameObject NormalAqua;
		public GameObject NormalEmerald;
		public GameObject NormalRuby;
		public GameObject NormalAmethyst;
		public GameObject NormalOpal;
		//
		public GameObject FlawlessDiamond;
		public GameObject FlawlessSaphire;
		public GameObject FlawlessTopaz;
		public GameObject FlawlessAqua;
		public GameObject FlawlessEmerald;
		public GameObject FlawlessRuby;
		public GameObject FlawlessAmethyst;
		public GameObject FlawlessOpal;
		//
		public GameObject PerfectDiamond;
		public GameObject PerfectSaphire;
		public GameObject PerfectTopaz;
		public GameObject PerfectAqua;
		public GameObject PerfectEmerald;
		public GameObject PerfectRuby;
		public GameObject PerfectAmethyst;
		public GameObject PerfectOpal;
		//
	
	[Header("Others")]
	public LayerMask TowerSelect;
	public LayerMask TowerPlacement;
	public LayerMask OpenMouseMask;
		
	public static GameObject GroundExit;
	public static GameObject GroundSpawn;
	public static GameObject AirSpawn;
	public static GameObject AirExit;
	
	int[][,] BlockedSpots;
		
	GameObject[] ChippedTowers;
	GameObject[] FlawedTowers;
	GameObject[] NormalTowers;
	GameObject[] FlawlessTowers;
	GameObject[] PerfectTowers;
	GameObject TowerToPlace;
	GameObject TempPlacer;
	
	GameObject   HoverTower;
	Tower        HoverTowerScript;	
	GameObject   SelectedTower;
	Tower		 SellectedTowerScript;
	GameObject   HoverButton;
	ButtonScript HoverButtonScript;	
	public static int GameMode;
	GameObject[,] TowerPrefabChoices;
	
	//Current GamePlay Settings
	int CurrentGold;
	int CurrentLives;
	int TotalKills;
	int[] Chances;
	int[] NextChances;
	int CurrentChacne;
	int CostToUpgrade;
	int level;
	int reallevel;
	int TowersToPlace;
	List<GameObject>TempTowers;
	List<GameObject> PartsFound;
	bool HasSelectedTower;
	bool FastPlayMode = false;
	int CurrentTimeMultip = 1;
	
	public static int CurrentCreepHP;	
	public static int CreepsSpawned;
	public static int CreepsKilled;
	public static int CreepsFinished;
	public static List<GameObject> CurrentCreeps;
	public static GameObject[,] TowersArray;	
	public static bool CanPlaceTower;

	float TimeOfNextCreep;
	GameObject TestCreep;
	NavMeshPath TestPath;

	GameObject SlateToMove;
	int ReturnToGameMode;

	//called at start of scene
	void Start(){
		Application.targetFrameRate = 60;
		BuildTowerArrays();
		TowersArray = new GameObject[60,60];
	
		GameMode = 1;
		CurrentCreeps = new List<GameObject>();			
		NewGame();
	}	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.I)){
			CurrentGold += 100;
		}
		//
		if(CurrentLives <= 0){
			NewGame();
		}
		BuildUI();
		//
		switch(GameMode){
		case 1 :
			OpenMouse();
			break;
		case 2 :
			GameMode2();
			break;
		case 3 :
			GameMode3();
			break;
		case 4 :
			MoveSlate();
			break;
		}
	}
	
	//Finds any waypoints, pathsquares, and makes them unbuildable (adds them to TowersArray)
	void BlockPath(){
		//find all waypoints & add them to tower array
		GameObject[] Waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
		foreach (GameObject WP in Waypoints){
			Vector3 tempWPpos = WP.gameObject.transform.position;			
			TowersArray[Mathf.RoundToInt(tempWPpos.z*2),Mathf.RoundToInt(tempWPpos.x*2)] = WP.gameObject;			
		}
	}
	
	public void NewGame(){

		GroundSpawn = GameObject.FindGameObjectWithTag("Spawner")as GameObject;
		GroundExit = GameObject.FindGameObjectWithTag("Exit")as GameObject;
		AirSpawn = GameObject.FindGameObjectWithTag("AirSpawn")as GameObject;
		AirExit = GameObject.FindGameObjectWithTag("AirExit")as GameObject;

		GameMode = 1;
		CurrentChacne = 0;
		ClearMap();		
		foreach(GameObject creep in CurrentCreeps){
			Destroy(creep);
		}
		CurrentCreeps = new List<GameObject>();
		CurrentCreepHP = startingCreepHP;
		TowersArray = new GameObject[60,60];
		TempTowers = new List<GameObject>();
		ClearMap();
		CurrentGold = 0;
		CurrentLives = StartingLives;
		level = 0;
		TotalKills = 0;
		BlockPath();
		NextLevel();
		ClearSelectedTower();
		ClearHoverTower();
	}

	void NextLevel(){
		level++;
		reallevel = level;
		BuildModeStart();

		//waypoint line colors (other ON OFF thing could replace the lines.)
		for (int i = 0; i < Waypoints.Length-1; i++){
			LineRenderer templine = Waypoints[i].GetComponent<LineRenderer>();
			templine.SetColors(Color.white,Color.green);
		}

		Debug.Log("LEVEL " + level + ": CreepHP = " + HpThisRound(level,startingCreepHP,CreepHpMultiplier));
		SetChances();
	}

	public void twoXTime(){
		CurrentTimeMultip = CurrentTimeMultip * 2;
		FastPlayMode = true;
	}

	public void oneXTime(){
		Time.timeScale = 1;
		FastPlayMode = false;
	}
	
	void BuildUI(){
		//play mode only things.
		if(GameMode == 3)
		{
			oneXTimeButton.SetActive(true);
			twoXTimeButton.SetActive(true);
		}else{
			oneXTimeButton.SetActive(false);
			twoXTimeButton.SetActive(false);
		}

		FillChances();

		CurrentGoldText.text = ("Gold : $" + CurrentGold);
		CurrentLivesText.text = ("Lives : " + CurrentLives);
		CurrentKillsText.text = ("Kills : " + TotalKills);
		if(CurrentChacne != ChanceUpgradeCosts.Length){
			CostToUpgrade = ChanceUpgradeCosts[CurrentChacne];
			CostToUpgradeChancesText.text = ("$" + CostToUpgrade);
			UpgradeChancesButton.SetActive(true);
		}else{
			CostToUpgradeChancesText.text = "Fully Upgraded";
			UpgradeChancesButton.SetActive(false);
		}			

		if(HasSelectedTower == true){
			TowerMenu.SetActive(true);

			DowngradeButton.SetActive(false);
			CombineButton.SetActive(false);
			SelectButton.SetActive(false);
			if(SellectedTowerScript.TempOrNot == true){				
				if(SellectedTowerScript.mySize.ToString() != "Chipped"){
					DowngradeButton.SetActive(true);
				}				
				if(CanCombine() == true){
					CombineButton.SetActive(true);
				}
				if(SellectedTowerScript.TempOrNot == true){
					SelectButton.SetActive(true);
				}	
			}	

			if(SellectedTowerScript.UpgradeCost != 0){
				UpgradeButton.SetActive(true);
				UpgradeCostText.text = SellectedTowerScript.UpgradeCost.ToString();
			}else{
				UpgradeButton.SetActive(false);
				UpgradeCostText.text = "";
			}
			UICheckSpecial(); //looks to see if all parts needed to make special are built, puts button up if special can be made!
			UICheckSlate();

			if(SellectedTowerScript.isSpecial == false){
				TowerName.text = SellectedTowerScript.mySize.ToString();
				TowerName.text += " " + SellectedTowerScript.myType.ToString();
			}else{
				TowerName.text = SellectedTowerScript.SpecialName;
			}

			if(SellectedTowerScript.isSlate == true){
				TowerName.text = SellectedTowerScript.SpecialName;
				if(SellectedTowerScript.SlateMoved == false){
					MoveSlateButton.SetActive(true);
				}else{
					MoveSlateButton.SetActive(false);
				}
			}else{
				MoveSlateButton.SetActive(false);
			}

			if(SellectedTowerScript.myType.ToString() != "Rock"){
				ReplaceBtn.SetActive(false);
				Turret CurrentTurret = SellectedTowerScript.myTurret.GetComponent<Turret>();
				SellButton.SetActive(false);
				TowerDmg.text = "" + CurrentTurret.MinDmg;
				TowerDmg.text += "-" + CurrentTurret.MaxDmg;
				TowerCooldown.text = "" + CurrentTurret.AttackCooldown;
				Turret tempturret = SellectedTowerScript.myTurret.GetComponent<Turret>();
				if(tempturret.percentCDReduction > 0){
					TowerCooldown.text += ("- " + tempturret.percentCDReduction + "%");
				}
				TowerRange.text = "" + CurrentTurret.AttackRange;

				if(SellectedTowerScript.HasSpecial == true){
					Tower tempspecial = SellectedTowerScript.mySpecial.GetComponent<Tower>();
					mySpecialName.text = tempspecial.SpecialName;
					mySpecialParts.text = "";
					for(int i = 0; i < tempspecial.Ingredients.Length; i++){
						mySpecialParts.text += (" " + tempspecial.Ingredients[i].name + ",");
					}
				}else{
					mySpecialName.text = "";
					mySpecialParts.text = "";
				}

			}else{

				if(TowersToPlace > 0){
					ReplaceBtn.SetActive(true);
					if(Input.GetButtonDown("Replace")){
						ReplaceMazeRock();
					}
				}else{
					ReplaceBtn.SetActive(false);
				}

				SellButton.SetActive(true);
				TowerDmg.text = "";
				TowerCooldown.text = "";
				TowerRange.text = "";
			}

		}else{

			WaveCountText.text =   "Current Wave : " + level.ToString();
			HpPerCreepText.text =  "HP per Creep : " + HpThisRound(level,startingCreepHP,CreepHpMultiplier).ToString();

			TowerMenu.SetActive(false);

			ReplaceBtn.SetActive(false);
			if(TowersToPlace > 0){
				BuildButton.SetActive(true);
			}else{
				BuildButton.SetActive(false);
			}
		}
	}
	
	bool CanCombine(){		
			foreach(GameObject temp in TempTowers){
				if(temp){
					Tower tempScript = temp.GetComponent<Tower>();
					if(temp != SelectedTower && SellectedTowerScript.mySize == tempScript.mySize && SellectedTowerScript.myType == tempScript.myType){
						return true;
					}
				}
			}		
		return false;
	}
		
	void BuildModeStart(){
		CanPlaceTower = true;
		TowersToPlace = 5;
		TempTowers = new List<GameObject>();		
		GameMode = 1;
	}
	
	void OpenMouse(){
		Ray MouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit MouseRayHit;

		if (Physics.Raycast (MouseRay, out MouseRayHit, 1000f, OpenMouseMask)) {	
			if(!EventSystem.current.IsPointerOverGameObject()){
				if(MouseRayHit.collider.gameObject.tag == "Tower" || MouseRayHit.collider.gameObject.tag == "Rock"){//mouse is over a tower;
					SetHoverTower(MouseRayHit.collider.gameObject);
					if(Input.GetMouseButtonDown(0)){//mouse was clicked while over tower.
						ClearSelectedTower();
						SellectedTowerScript = HoverTowerScript;
						SellectedTowerScript.Select(true);
						SelectedTower = HoverTower;
						HasSelectedTower = true;			
						ClearHoverTower();
					}
				}else if(MouseRayHit.collider.gameObject.tag == "Ground"){
					ClearHoverTower();
					ClearHoverButton();
					if(Input.GetMouseButtonDown(0)){//mouse was clicked while over ground.				
						ClearSelectedTower();
					}
				}else if(MouseRayHit.collider.gameObject.tag == "Button"){
					ClearHoverTower();
					SetHoverButton(MouseRayHit.collider.gameObject);
					
					if(Input.GetMouseButtonDown(0)){
						HoverButtonScript.Clicked();
					}
				}else{
					ClearHoverTower();
					ClearHoverButton();
					if(Input.GetMouseButtonDown(0)){
						ClearSelectedTower();
					}
				}
			}
		}
	}

	void GameMode3(){

		if(FastPlayMode == true){
			if(CurrentTimeMultip <= 2){
				Time.timeScale = CurrentTimeMultip;
			}else{
				Time.timeScale = 2;
			}
		}else{
			Time.timeScale = 1;
		}

		if(CreepsSpawned < CreepsPerWave && Time.time >= TimeOfNextCreep){
			GameObject SpawnToUse;
			GameObject ExitToUse;
			GameObject NewCreep;
			//get creep settings
			if(level % 4 == 0){
				//air round
				SpawnToUse = AirSpawn;
				ExitToUse = AirExit;
				NewCreep = AirCreeps[Random.Range(0,AirCreeps.Length)];
			}else{
				//ground round
				SpawnToUse = GroundSpawn;
				ExitToUse = GroundExit;
				NewCreep = GroundCreeps[Random.Range(0,GroundCreeps.Length)];
			}

			//spawn new creep
			GameObject tempCreep = Instantiate(NewCreep,SpawnToUse.transform.position,Quaternion.identity)as GameObject;
			tempCreep.name = "Creep" + CreepsSpawned;
			//give creep correct settings
			Creep tempCreepScript = tempCreep.GetComponent<Creep>();

			tempCreepScript.MaxHP = HpThisRound(level,startingCreepHP,CreepHpMultiplier);
			tempCreepScript.HP = tempCreepScript.MaxHP;
			tempCreepScript.ExitObj = ExitToUse;
			CurrentCreeps.Add(tempCreep);	
			//reset creep spawn clock & tell game we spawned a creep
			TimeOfNextCreep = Time.time + SecondsBetweenCreeps;
			CreepsSpawned++;
		}	
			
		if(CreepsKilled + CreepsFinished  == CreepsPerWave){
			CurrentCreeps = new List<GameObject>();
			CreepsKilled = 0;
			GameMode = 1;
			NextLevel();
		}		
		OpenMouse();
	}
	
	void GameMode2(){
		Ray MouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);;
		RaycastHit MouseRayHit;
		float TempX;
		float TempZ;
		if(Physics.Raycast(MouseRay,out MouseRayHit, 100f, TowerPlacement)){			
			TempX = (Mathf.Round(MouseRayHit.point.x *2))/2;
			TempZ = (Mathf.Round(MouseRayHit.point.z *2))/2;
			Vector3 TempLoc = new Vector3(TempX,0f,TempZ);

						
			Destroy(TempPlacer);					
			TempPlacer = Instantiate(TowerPlacer,TempLoc,Quaternion.identity) as GameObject;								
			
			TempPlacer.transform.position = TempLoc;
			if(Input.GetMouseButtonDown(0)){
				if(!EventSystem.current.IsPointerOverGameObject()){
					Tower testtower = TowerToPlace.GetComponent<Tower>();
					//if(testtower.CanBuildOnMe == false || TowersArray[Mathf.RoundToInt(TempZ*2),Mathf.RoundToInt(TempX)*2] != null){
					//	CanPlaceTower = false;	
					//}						
					if(isBlocked(Mathf.RoundToInt(TempZ*2),Mathf.RoundToInt(TempX*2)) == false || testtower.CanBuildOnMe == true){
						if(CanPlaceTower){								
								TowersToPlace--;
								
								TowersArray[Mathf.RoundToInt(TempZ*2),Mathf.RoundToInt(TempX*2)] = Instantiate(TowerToPlace,TempPlacer.transform.position,Quaternion.identity) as GameObject;

								TempTowers.Add (TowersArray[Mathf.RoundToInt(TempZ*2),Mathf.RoundToInt(TempX*2)]);
								//Debug.Log("TOWER ARRAY @ " + (Mathf.RoundToInt(TempZ*2)) + "x" + (Mathf.RoundToInt(TempX*2)) + " FILLED");
														
								Destroy(TempPlacer);


							//
							if(Pathclear() == false){
								TempTowers.Remove(TowersArray[Mathf.RoundToInt(TempZ*2),Mathf.RoundToInt(TempX*2)]);
								Destroy(TowersArray[Mathf.RoundToInt(TempZ*2),Mathf.RoundToInt(TempX*2)]);
								TowersArray[Mathf.RoundToInt(TempZ*2),Mathf.RoundToInt(TempX*2)] = null;
								TowersToPlace++;
							}
							//


							if(/*Input.GetKey(KeyCode.LeftShift) && */TowersToPlace > 0){
								PlaceRandomTower();

							}else{
								GameMode = 1;
								//Debug.Log("GameMode1");
							}
						}
					}
				}
			}
			
		}
		if(Input.GetMouseButtonDown(1)){
			Destroy(TempPlacer);
			GameMode = 1;		
		}
	}
	
	public void SelectThisTower(){	
		foreach(GameObject tower in TempTowers){
			if(tower != SelectedTower){
				Vector3 tempcords = tower.transform.position;
				Destroy(tower);
				if(tempcords != SelectedTower.transform.position){
					TowersArray[Mathf.RoundToInt(tempcords.z*2),Mathf.RoundToInt(tempcords.x*2)] = Instantiate(MazeRock,tempcords,Quaternion.identity)as GameObject;	
				}
			}			
		}
		SellectedTowerScript.TempOrNot = false;
		TempTowers = null;
		if(GameMode != 3){
			PlayModeStart();
		}
	}

	public void TryToUpgradeChances(){
		if(CurrentGold >= CostToUpgrade){
			CurrentGold -= CostToUpgrade;
			CurrentChacne++;
			SetChances();
		}
	}
	
	public void Downgrade(){		
		GameObject FutureTower = SellectedTowerScript.DowngradeObj;
		Vector3 FuturePos = SelectedTower.transform.position;
		TempTowers.Remove(SelectedTower);			 
		Destroy(SelectedTower.gameObject);
		TowersArray[Mathf.RoundToInt(FuturePos.z*2),Mathf.RoundToInt(FuturePos.x*2)] = Instantiate(FutureTower,FuturePos,Quaternion.identity)as GameObject;
		SelectedTower = TowersArray[Mathf.RoundToInt(FuturePos.z*2),Mathf.RoundToInt(FuturePos.x*2)];
		SellectedTowerScript = SelectedTower.GetComponent<Tower>();
		SelectThisTower();
	}
	
	public void CombineGems(){		
		GameObject FutureTower = SellectedTowerScript.CombineObj;
		Vector3 FuturePos = SelectedTower.transform.position;					 
		TempTowers.Remove(SelectedTower);		
		Destroy(SelectedTower.gameObject);
		ClearSelectedTower();
		TowersArray[Mathf.RoundToInt(FuturePos.z*2),Mathf.RoundToInt(FuturePos.x*2)] = Instantiate(FutureTower,FuturePos,Quaternion.identity)as GameObject;
		SelectedTower = TowersArray[Mathf.RoundToInt(FuturePos.z*2),Mathf.RoundToInt(FuturePos.x*2)];
		SellectedTowerScript = SelectedTower.GetComponent<Tower>();
		SellectedTowerScript.TempOrNot = false;
		SelectThisTower();
		ClearSelectedTower();
	}

	public void UpgradeSpecial(){
		if(SellectedTowerScript.SpecialUpgrade && CurrentGold >= SellectedTowerScript.UpgradeCost){
			GameObject tempObj = SellectedTowerScript.SpecialUpgrade;
			Vector3 temploc = SelectedTower.transform.position;
			CurrentGold -= SellectedTowerScript.UpgradeCost;
			SellTower();
			TowersArray[Mathf.RoundToInt(temploc.z*2),Mathf.RoundToInt(temploc.x*2)] = Instantiate (tempObj,temploc,Quaternion.identity)as GameObject;


		}
	}

	public void MoveThisSlate(){
		SlateToMove = SelectedTower;
		ReturnToGameMode = GameMode;
		Time.timeScale = 0;
		GameMode = 4;
	}

	void MoveSlate(){
		Ray MouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);;
		RaycastHit MouseRayHit;
		float TempX;
		float TempZ;
		if(Physics.Raycast(MouseRay,out MouseRayHit, 100f, TowerPlacement)){			
			TempX = (Mathf.Round(MouseRayHit.point.x *2))/2;
			TempZ = (Mathf.Round(MouseRayHit.point.z *2))/2;
			Vector3 TempLoc = new Vector3(TempX,0f,TempZ);
			
			
			Destroy(TempPlacer);					
			TempPlacer = Instantiate(TowerPlacer,TempLoc,Quaternion.identity) as GameObject;								
			
			TempPlacer.transform.position = TempLoc;
			if(Input.GetMouseButtonDown(0)){
				if(!EventSystem.current.IsPointerOverGameObject()){
					Tower testtower = SlateToMove.GetComponent<Tower>();
					if(TowersArray[Mathf.RoundToInt(TempZ*2),Mathf.RoundToInt(TempX)*2] == null){
						CanPlaceTower = true;	
					}else{
						CanPlaceTower = false;
					}
					if(testtower.CanBuildOnMe == true){
						if(CanPlaceTower){
							TowersArray[Mathf.RoundToInt((SlateToMove.transform.position.z *2)/2),Mathf.RoundToInt((SlateToMove.transform.position.x *2)/2)] = null;
							SlateToMove.transform.position = TempPlacer.transform.position;
							TowersArray[Mathf.RoundToInt(TempZ*2),Mathf.RoundToInt(TempX*2)] = TowerToPlace;							
							Destroy(TempPlacer);
							Tower tempTower = SlateToMove.GetComponent<Tower>();
							tempTower.SlateMoved = true;
							Time.timeScale = 1;
							GameMode = ReturnToGameMode;
						}
					}
				}
			}
			
		}
		if(Input.GetMouseButtonDown(1)){
			Destroy(TempPlacer);
			Time.timeScale = 1;
			GameMode = ReturnToGameMode;	
		}

	}
	
	public void PlaceSpecial(){
		Time.timeScale = 0;
		GameObject FutureTower = SellectedTowerScript.mySpecial;
		Vector3 FuturePos = SelectedTower.transform.position;
		//SellTower();
		if(SellectedTowerScript.TempOrNot == true){
			SelectThisTower();
		}else if(PartsFound.Count > 0){
			foreach(GameObject part in PartsFound){
				TowersArray[Mathf.RoundToInt(part.transform.position.z*2),Mathf.RoundToInt(part.transform.position.x*2)] = Instantiate(MazeRock,part.transform.position,Quaternion.identity)as GameObject;
				Destroy(part);
			}
		}

		Destroy(TowersArray[Mathf.RoundToInt(FuturePos.z*2),Mathf.RoundToInt(FuturePos.x*2)]);
		TowersArray[Mathf.RoundToInt(FuturePos.z*2),Mathf.RoundToInt(FuturePos.x*2)] = Instantiate(FutureTower,FuturePos,Quaternion.identity)as GameObject;
		SelectedTower = TowersArray[Mathf.RoundToInt(FuturePos.z*2),Mathf.RoundToInt(FuturePos.x*2)];
		SellectedTowerScript = SelectedTower.GetComponent<Tower>();
		SellectedTowerScript.TempOrNot = false;
		HasSelectedTower = true;

		ClearSelectedTower();
	}

	public void PlaceSlate(){
		GameObject FutureTower = SellectedTowerScript.mySlate;
		Vector3 FuturePos = SelectedTower.transform.position;
		//SellTower();
		if(TempTowers != null){
			SelectThisTower();
		}else if(PartsFound.Count > 0){
			foreach(GameObject part in PartsFound){
				TowersArray[Mathf.RoundToInt(part.transform.position.z*2),Mathf.RoundToInt(part.transform.position.x*2)] = Instantiate(MazeRock,part.transform.position,Quaternion.identity)as GameObject;
				Destroy(part);
			}
		}
		
		Destroy(TowersArray[Mathf.RoundToInt(FuturePos.z*2),Mathf.RoundToInt(FuturePos.x*2)]);
		TowersArray[Mathf.RoundToInt(FuturePos.z*2),Mathf.RoundToInt(FuturePos.x*2)] = Instantiate(FutureTower,FuturePos,Quaternion.identity)as GameObject;
		SelectedTower = TowersArray[Mathf.RoundToInt(FuturePos.z*2),Mathf.RoundToInt(FuturePos.x*2)];
		SellectedTowerScript = SelectedTower.GetComponent<Tower>();
		SellectedTowerScript.TempOrNot = false;
		HasSelectedTower = true;
		
		ClearSelectedTower();
		Time.timeScale = 1;
	}

	public void ReplaceMazeRock(){
		if(SellectedTowerScript.myType.ToString() == "Rock"){
			Vector3 tempcords = SelectedTower.transform.position;
			int RandomNumber = Random.Range(0,100);//not very random
			
			if(RandomNumber == 0){
				TowerToPlace = ChippedTowers[Random.Range(0,ChippedTowers.Length)];
			}
			else if(RandomNumber <= Chances[4]){
				TowerToPlace = PerfectTowers[Random.Range(0,PerfectTowers.Length)];
			}
			else if(RandomNumber <= Chances[3]){
				TowerToPlace = FlawlessTowers[Random.Range(0,FlawlessTowers.Length)];
			}
			else if(RandomNumber <= Chances[2]){
				TowerToPlace = NormalTowers[Random.Range(0,NormalTowers.Length)];
			}
			else if(RandomNumber <= Chances[1]){
				TowerToPlace = FlawedTowers[Random.Range(0,FlawedTowers.Length)];
			}else{
				TowerToPlace = ChippedTowers[Random.Range(0,ChippedTowers.Length)];
			}

			SellTower();
			TowersArray[Mathf.RoundToInt(tempcords.z*2),Mathf.RoundToInt(tempcords.x*2)] = Instantiate(TowerToPlace,tempcords,Quaternion.identity)as GameObject;
			TowersToPlace--;
			TempTowers.Add (TowersArray[Mathf.RoundToInt(tempcords.z*2),Mathf.RoundToInt(tempcords.x*2)]);
		}
	}	
	public void PlaceRandomTower(){
		int RandomNumber = Random.Range(0,100);//not very random

		if(RandomNumber == 0){
			TowerToPlace = ChippedTowers[Random.Range(0,ChippedTowers.Length)];
		}
		else if(RandomNumber <= Chances[4]){
			TowerToPlace = PerfectTowers[Random.Range(0,PerfectTowers.Length)];
		}
		else if(RandomNumber <= Chances[3]){
			TowerToPlace = FlawlessTowers[Random.Range(0,FlawlessTowers.Length)];
		}
		else if(RandomNumber <= Chances[2]){
			TowerToPlace = NormalTowers[Random.Range(0,NormalTowers.Length)];
		}
		else if(RandomNumber <= Chances[1]){
			TowerToPlace = FlawedTowers[Random.Range(0,FlawedTowers.Length)];
		}else{
			TowerToPlace = ChippedTowers[Random.Range(0,ChippedTowers.Length)];
		}
		
		GameMode = 2;
	}
	public void ToggleHelpScreen(){
		if(HelpPanel.activeInHierarchy == true){
			HelpPanel.SetActive(false);
		}else{
			HelpPanel.SetActive(true);
		}
	}
	public void ToggleOptionsScreen(){
		if(OptionsPanel.activeInHierarchy == true){
			OptionsPanel.SetActive(false);
		}else{
			OptionsPanel.SetActive(true);
		}
	}
	public void ClearMap(){
		for(int Z = 0; Z < 60; Z++){
			for(int X = 0; X < 60; X++){
				if(TowersArray[Z,X]){
				if(TowersArray[Z,X].tag == "Tower" || TowersArray[Z,X].tag == "Rock"){					
						Destroy(TowersArray[Z,X]);			
					}
				}		
			}
		}
		//if there are any rocks left on board, destory them!
		if(GameObject.FindGameObjectWithTag("Rock")){
			GameObject[] Trash = GameObject.FindGameObjectsWithTag("Rock");
			foreach (GameObject rock in Trash){
				Destroy(rock);
			}
		}
	}

	void PlayModeStart(){
		TowerMenu.SetActive(false);
		ClearHoverTower();
		ClearSelectedTower();
		if(TempPlacer){Destroy(TempPlacer);}
		TimeOfNextCreep = Time.time;
		CreepsFinished = 0;
		CreepsKilled = 0;
		CreepsSpawned = 0;
		CurrentCreeps = new List<GameObject>();
		GameMode = 3;
	}
	
	public void SellTower(){
		TowersArray[Mathf.RoundToInt( SelectedTower.transform.position.x),Mathf.RoundToInt( SelectedTower.transform.position.z)] = null;
		Destroy(SelectedTower);
		ClearSelectedTower();
	}
	
	public void KillCreep(GameObject creep){
		if(level < 4){
			CurrentGold += 1;
		}else if(level < 8){
			CurrentGold += 2;
		}else{
			CurrentGold += (level / 4) + GoldPerCreepper4levels;
		}
		CurrentCreeps.Remove(creep);
		CreepsKilled++;
		TotalKills++;
		Destroy(creep);
	}	
	
	public void DeSpawnCreep(GameObject creep){
		CurrentCreeps.Remove(creep);
		CurrentLives--;
		CreepsFinished++;
		Destroy(creep);
	}

	int HpThisRound(int Round, int BaseHP,double multip){
		if(Round <= 1){
			return BaseHP;
		}else if(Round == 2){
			return 2 * BaseHP;
		}else if(level % 4 == 0){
			return (int)((level * BaseHP));
		}else{
			//Debug.Log(Round);
			return (int)((multip - ((level / 25)*.025)) * HpThisRound(Round -1,BaseHP,multip));
		}
	}

	bool isBlocked(int Z, int X){
		if(Z < 0 || X < 0){
			return true;
		}
		for(int tZ = Z-1; tZ < Z+2; tZ++){
			for(int tX = X-1; tX < X+2; tX++){
				if(tX < 0){
					tX = 0;
				}
				if(tZ < 0){
					tZ = 0;
				}
				if(TowersArray[tZ,tX]){
					Tower tempTowerScript = TowersArray[tZ,tX].gameObject.GetComponent<Tower>();					
					if(tempTowerScript.CanBuildOnMe == false){					
						return true;
					}
				}
				
			}
		}
		return false;
	}
	void SetHoverButton(GameObject NewButton){
		if(HoverButton && HoverButton != NewButton){
			ClearHoverButton();
			HoverButton = NewButton;
			HoverButtonScript = NewButton.GetComponent<ButtonScript>();
			HoverButtonScript.Hover();
		}else{
			HoverButton = NewButton;
			HoverButtonScript = NewButton.GetComponent<ButtonScript>();
			HoverButtonScript.Hover();
		}
	}
	
	void SetHoverTower(GameObject NewTower){
		if (HoverTower && HoverTower != NewTower) {
			ClearHoverTower();
			HoverTower = NewTower;
			HoverTowerScript = HoverTower.GetComponent<Tower>();
			HoverTowerScript.Hover(true);
		} else{
			HoverTower = NewTower;
			HoverTowerScript = HoverTower.GetComponent<Tower> ();
			HoverTowerScript.Hover (true);
		}
	}
	
	void ClearHoverTower(){
		if(HoverTower != null){
			HoverTowerScript.Hover(false);
			HoverTower = null;
			HoverTowerScript = null;
		}
	}
	void ClearSelectedTower(){						
		if(HasSelectedTower == true){
			SellectedTowerScript.Select(false);
			SelectedTower = null;
			SellectedTowerScript = null;
			HasSelectedTower = false;
		}						
	}
	void ClearHoverButton(){
		if (HoverButton != null) {
			HoverButtonScript.Exit();
			HoverButtonScript = null;
			HoverButton = null;
		}
	}

	bool Pathclear(){
		TestPath = new NavMeshPath();
		float TotalPath = 0;
		//start
		NavMesh.CalculatePath(GroundSpawn.transform.position,Waypoints[0].transform.position,NavMesh.AllAreas,TestPath);
		if(Vector3.Distance(TestPath.corners[TestPath.corners.Length-1], Waypoints[0].transform.position) > .4f){
			return false;
		}
		for(int i = 0; i < TestPath.corners.Length -1; i++){
			TotalPath += Vector3.Distance(TestPath.corners[i],TestPath.corners[i+1]);
			Debug.DrawLine(TestPath.corners[i],TestPath.corners[i+1],Color.cyan,1.0f);
		}
		//middle
		for (int WP = 0; WP < Waypoints.Length-2; WP++){
			NavMesh.CalculatePath(Waypoints[WP].transform.position,Waypoints[WP+1].transform.position,NavMesh.AllAreas,TestPath);
			for(int i = 0; i < TestPath.corners.Length -1; i++){
				TotalPath += Vector3.Distance(TestPath.corners[i],TestPath.corners[i+1]);
				Debug.DrawLine(TestPath.corners[i],TestPath.corners[i+1],Color.cyan,1.0f);
			}
			if(Vector3.Distance(TestPath.corners[TestPath.corners.Length-1], Waypoints[WP+1].transform.position) > .4f){
				return false;
			}
		}
		//EXIT
		NavMesh.CalculatePath(Waypoints[Waypoints.Length-1].transform.position,GroundExit.transform.position ,NavMesh.AllAreas,TestPath);
		if(Vector3.Distance(TestPath.corners[TestPath.corners.Length-1], GroundExit.transform.position) > .4f){
			return false;
		}
		for(int i = 0; i < TestPath.corners.Length -1; i++){
			TotalPath += Vector3.Distance(TestPath.corners[i],TestPath.corners[i+1]);
			Debug.DrawLine(TestPath.corners[i],TestPath.corners[i+1],Color.cyan,1.0f);
		}


		Debug.Log("PATH LENGTH TOTAL = " + TotalPath);
		
		return true;
	}

	void BuildTowerArrays(){
		ChippedTowers = new GameObject[8];
		FlawedTowers = new GameObject[8];
		NormalTowers = new GameObject[8];
		FlawlessTowers = new GameObject[8];
		PerfectTowers = new GameObject[8];		
		
		ChippedTowers[0] = ChippedDiamond;
		ChippedTowers[1] = ChippedTopaz;
		ChippedTowers[2] = ChippedSaphire;
		ChippedTowers[3] = ChippedEmerald;
		ChippedTowers[4] = ChippedRuby;
		ChippedTowers[5] = ChippedAqua;
		ChippedTowers[6] = ChippedAmethyst;
		ChippedTowers[7] = ChippedOpal;
		
		FlawedTowers[0] = FlawedDiamond;
		FlawedTowers[1] = FlawedTopaz;
		FlawedTowers[2] = FlawedSaphire;
		FlawedTowers[3] = FlawedEmerald;
		FlawedTowers[4] = FlawedRuby;
		FlawedTowers[5] = FlawedAqua;
		FlawedTowers[6] = FlawedAmethyst;
		FlawedTowers[7] = FlawedOpal;
		
		NormalTowers[0] = NormalDiamond;
		NormalTowers[1] = NormalTopaz;
		NormalTowers[2] = NormalSaphire;
		NormalTowers[3] = NormalEmerald;
		NormalTowers[4] = NormalRuby;
		NormalTowers[5] = NormalAqua;
		NormalTowers[6] = NormalAmethyst;
		NormalTowers[7] = NormalOpal;
		
		FlawlessTowers[0] = FlawlessDiamond;
		FlawlessTowers[1] = FlawlessTopaz;
		FlawlessTowers[2] = FlawlessSaphire;
		FlawlessTowers[3] = FlawlessEmerald;
		FlawlessTowers[4] = FlawlessRuby;
		FlawlessTowers[5] = FlawlessAqua;
		FlawlessTowers[6] = FlawlessAmethyst;
		FlawlessTowers[7] = FlawlessOpal;
	
		PerfectTowers[0] = PerfectDiamond;
		PerfectTowers[1] = PerfectTopaz;
		PerfectTowers[2] = PerfectSaphire;
		PerfectTowers[3] = PerfectEmerald;
		PerfectTowers[4] = PerfectRuby;
		PerfectTowers[5] = PerfectAqua;
		PerfectTowers[6] = PerfectAmethyst;
		PerfectTowers[7] = PerfectOpal;		
	}
	void FillChances(){
		SetChances();
		CurrentChipped.text = Chances[0].ToString();
		CurrentFlawed.text = Chances[1].ToString();
		CurrentNormal.text = Chances[2].ToString();
		CurrentFlawless.text = Chances[3].ToString();
		CurrentPerfect.text = Chances[4].ToString();
		NextChipped.text = NextChances[0].ToString();
		NextFlawed.text = NextChances[1].ToString();
		NextNormal.text = NextChances[2].ToString();
		NextFlawless.text = NextChances[3].ToString();
		NextPerfect.text = NextChances[4].ToString();
	}
	void SetChances(){
		switch(CurrentChacne){
		case 0:
			Chances = Odds0;
			NextChances = Odds1;
			break;
		case 1:
			Chances = Odds1;
			NextChances = Odds2;
			break;
		case 2:
			Chances = Odds2;
			NextChances = Odds3;
			break;
		case 3:
			Chances = Odds3;
			NextChances = Odds4;
			break;
		case 4:
			Chances = Odds4;
			NextChances = Odds5;
			break;
		case 5:
			Chances = Odds5;
			NextChances = Odds6;
			break;
		case 6:
			Chances = Odds6;
			NextChances = Odds7;
			break;
		case 7:
			Chances = Odds7;
			NextChances = Odds8;
			break;
		case 8:
			Chances = Odds8;
			NextChances = Odds9;
			break;
		case 9:
			Chances = Odds9;
			NextChances = Odds9;
			break;
		default:
			Chances = Odds0;
			NextChances = Odds1;
			break;
		};
	}

	void UICheckSpecial(){
		if(SellectedTowerScript.HasSpecial == true){
			PartsFound = new List<GameObject>();
			GameObject curSpecialObj = SellectedTowerScript.mySpecial;
			Tower curSpecial = curSpecialObj.GetComponent<Tower>();
			foreach(GameObject Ing in curSpecial.Ingredients){
				Tower curIng = Ing.GetComponent<Tower>();
				if(SellectedTowerScript.TempOrNot == false){
					foreach(GameObject builtTower in TowersArray){
						if(builtTower){
							if(builtTower.tag == "Tower"){
								Tower curBuilt = builtTower.GetComponent<Tower>();
								if(curBuilt.mySize == curIng.mySize && curBuilt.myType == curIng.myType){
									PartsFound.Add(builtTower);
									break;
								}
							}
						}
					}
				}else{
					foreach(GameObject builtTower in TempTowers){
						if(builtTower){
							if(builtTower.tag == "Tower"){
								Tower curBuilt = builtTower.GetComponent<Tower>();
								if(curBuilt.mySize == curIng.mySize && curBuilt.myType == curIng.myType){
									PartsFound.Add(builtTower);
									break;
								}
							}
						}
					}
					
				}
			}
			
			if(PartsFound.Count == curSpecial.Ingredients.Length){
				SpecialButton.SetActive(true);
			}else{
				SpecialButton.SetActive(false);
			}	
			
		}else{
			SpecialButton.SetActive(false);
		}
	}

	void UICheckSlate(){
		if(SellectedTowerScript.HasSlate == true){
			PartsFound = new List<GameObject>();
			GameObject curSpecialObj = SellectedTowerScript.mySlate;
			Tower curSpecial = curSpecialObj.GetComponent<Tower>();
			foreach(GameObject Ing in curSpecial.Ingredients){
				Tower curIng = Ing.GetComponent<Tower>();
				if(SellectedTowerScript.TempOrNot == false){
					foreach(GameObject builtTower in TowersArray){
						if(builtTower){
							if(builtTower.tag == "Tower"){
								Tower curBuilt = builtTower.GetComponent<Tower>();
								if(curBuilt.mySize == curIng.mySize && curBuilt.myType == curIng.myType){
									PartsFound.Add(builtTower);
									break;
								}
							}
						}
					}
				}else{
					foreach(GameObject builtTower in TempTowers){
						if(builtTower){
							if(builtTower.tag == "Tower"){
								Tower curBuilt = builtTower.GetComponent<Tower>();
								if(curBuilt.mySize == curIng.mySize && curBuilt.myType == curIng.myType){
									PartsFound.Add(builtTower);
									break;
								}
							}
						}
					}
					
				}
			}
			
			if(PartsFound.Count == curSpecial.Ingredients.Length){
				SlateButton.SetActive(true);
			}else{
				SlateButton.SetActive(false);
			}	
			
		}else{
			SlateButton.SetActive(false);
		}
	}
}
