﻿using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour {
	
	public bool LooktoCamera;
	
	void Update () {
		if(LooktoCamera){
			//transform.LookAt((Camera.main.transform.position - Vector3.up));
			transform.rotation = Camera.main.transform.rotation;
		}
		
	}
}
