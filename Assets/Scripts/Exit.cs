﻿using UnityEngine;
using System.Collections;

public class Exit : MonoBehaviour {

	GameObject MasterObj;
	GameMaster MAsterScript;
	
	// Use this for initialization
	void Start () {
		MasterObj = GameObject.FindGameObjectWithTag("GameMaster");
		MAsterScript = MasterObj.GetComponent<GameMaster>();
	}

	void OnTriggerEnter(Collider col){
		if(col.gameObject.tag == "Creep"){
			MAsterScript.DeSpawnCreep(col.gameObject);
		}
	}
}
