﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

	public float MaxY;
	public float MinY;
	public float MaxX;
	public float MinX;
	public float MaxZ;
	public float MinZ;
	public float speed;

	void Update () {
		//CAMERA MOVEMENT
		Vector3 Movement = new Vector3(Input.GetAxis("Horizontal"),Input.GetAxis("Mouse ScrollWheel")*-40,Input.GetAxis("Vertical"));
		Camera.main.transform.position += Movement * Time.deltaTime * speed;
		//CAMERA DRAG
		if(Input.GetMouseButton(2)){
			Vector3 newpos = new Vector3(Input.GetAxis("Mouse X") * Time.deltaTime * speed, 0, Input.GetAxis("Mouse Y") * Time.deltaTime * speed);
			
			this.transform.position -= newpos;
			
		}
		//check limits
		Vector3 NewPos = transform.position;

		if(transform.position.y > MaxY){
			NewPos.y = MaxY;
		}else if(transform.position.y < MinY){
			NewPos.y = MinY;
		}

		if(transform.position.x > MaxX){
			NewPos.x = MaxX;
		}else if(transform.position.x < MinX){
			NewPos.x = MinX;
		}

		if(transform.position.z > MaxZ){
			NewPos.z = MaxZ;
		}else if(transform.position.z < MinZ){
			NewPos.z = MinZ;
		}

		transform.position = NewPos;
	}
}
