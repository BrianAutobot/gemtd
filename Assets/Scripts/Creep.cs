﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Creep : MonoBehaviour {
	
	public bool Ground;
	public bool Air;
	public float Speed;	
	public int Reward;	
	public Slider HpSlider;
	public Text DmgDisplay;
	public float DmgDisplayDuration;
	
	public GameObject CreepObj;
	public int MaxHP;
	public int HP;
	float DmgDisplayOffTime;

	bool isTrapped;
	float TimeOfTrapEnd;

	bool isPoisoned;
	float TimePoisonOver;
	float PoisonCooldown;
	int poisonDmg;
	float NextPoisonDmg;
	
	bool isSlowed;
	float TimeEndSlow;
	float SlowSpeed;
	
	GameObject MasterObj;
	GameMaster MAsterScript;

	int CurrentWaypoint;
	Vector3 newDes;
	public GameObject ExitObj;
	public NavMeshAgent myAgent;	
	
	// Use this for initialization
	void Start () {
		CurrentWaypoint = 0;
		MasterObj = GameObject.FindGameObjectWithTag("GameMaster");
		MAsterScript = MasterObj.GetComponent<GameMaster>();
		myAgent = GetComponent<NavMeshAgent>();

		if(Air == true){
			newDes = MAsterScript.Waypoints[CurrentWaypoint].transform.position + (Vector3.up * 3);
		}else{
			newDes = MAsterScript.Waypoints[CurrentWaypoint].transform.position;
		}
		myAgent.SetDestination(newDes);
		myAgent.speed = Speed;
		DmgDisplay.enabled = false;		
	}
	
	void Update(){
		HpSlider.maxValue = MaxHP;
		HpSlider.value = HP;
		if(Vector3.Distance(myAgent.transform.position,myAgent.destination) < .2f){
			if(CurrentWaypoint < MAsterScript.Waypoints.Length-1){
				LineRenderer tempLine = MAsterScript.Waypoints[CurrentWaypoint].GetComponent<LineRenderer>();
				tempLine.SetColors(Color.white,Color.red);
				tempLine = MAsterScript.Waypoints[CurrentWaypoint+1].GetComponent<LineRenderer>();
			}

			CurrentWaypoint++;
			if(CurrentWaypoint > MAsterScript.Waypoints.Length-1){

				if(Air == true){
					newDes = ExitObj.transform.position;
				}else{
					newDes = ExitObj.transform.position;
				}
			}else{
				if(Air == true){
					newDes = MAsterScript.Waypoints[CurrentWaypoint].transform.position + (Vector3.up * 3);
				}else{
					newDes = MAsterScript.Waypoints[CurrentWaypoint].transform.position;
				}
			}

			myAgent.SetDestination(newDes);

		}
		
		if(DmgDisplay.enabled == true){
			if(Time.time >= DmgDisplayOffTime){
				DmgDisplay.enabled = false;
			}
		}
		
		if(isPoisoned){
			if(Time.time >= NextPoisonDmg){
				TakeDmg(poisonDmg);
				NextPoisonDmg = Time.time + PoisonCooldown;
			}
			
			if(Time.time >= TimePoisonOver){
				isPoisoned = false;
				//hide poison
			}
		}
		
		if(isSlowed){
			if(Time.time >= TimeEndSlow){
				myAgent.speed = Speed;
				isSlowed = false;
				//hide slow
			}
		}

		if (isTrapped) {
			if(Time.time >= TimeOfTrapEnd){
				myAgent.speed = Speed;
				isTrapped = false;
				//hide trap
			}
		}
	}

	public void Trap(float duration){
		myAgent.speed = 0;
		TimeOfTrapEnd = Time.time + duration;
			isTrapped = true;
			//show trap
	}
	
	public void Poison(int dmg, float duration, float cooldown){
		NextPoisonDmg = Time.time;
		poisonDmg = dmg;
		TimePoisonOver = Time.time + duration;
		PoisonCooldown = cooldown;
		isPoisoned = true;
		//show poison
	}
	
	public void Slow(int percent, float duration){
		isSlowed = true;
		SlowSpeed = ((100f-percent) / 100f) * Speed;
		TimeEndSlow = Time.time + duration;
		myAgent.speed = SlowSpeed;
		//show slow
	}	
	
	public void TakeDmg(int dmg){
		HP -= dmg;
		HpSlider.value = HP;
		DmgDisplay.text = ""; DmgDisplay.text += dmg;
		DmgDisplay.enabled = true;
		DmgDisplayOffTime = Time.time + DmgDisplayDuration;
		if(HP <= 0){
			MAsterScript.KillCreep(this.gameObject);
		}
	}	
	
}
