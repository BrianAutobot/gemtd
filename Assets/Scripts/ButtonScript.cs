﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.Events;

public class ButtonScript : MonoBehaviour {
	
	[Serializable]
	public class MyEventType : UnityEvent { }	
	public MyEventType OnClick;
	public MyEventType OnHover;
	public MyEventType OnExit;

	public void Clicked(){
		OnClick.Invoke();
	}
	public void Hover(){
		OnHover.Invoke();
	}
	public void Exit(){
		OnExit.Invoke ();
	}
}
