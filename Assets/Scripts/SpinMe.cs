﻿using UnityEngine;
using System.Collections;

public class SpinMe : MonoBehaviour {
	public float YSpeed;
	public float XSpeed;
	public float ZSpeed;
	// Update is called once per frame

	void Update () {
		transform.Rotate(new Vector3(XSpeed*Time.deltaTime,YSpeed*Time.deltaTime,ZSpeed*Time.deltaTime));
	}
}
