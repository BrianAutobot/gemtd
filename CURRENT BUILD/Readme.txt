Welcome to the PRE-ALPHA of Rimrock Games' "Gem Tower Defense"

This game is not finished, not even close! I have tons of work to do on this game before it is close to where I want it. 
My goal for this game is to make a close as possible clone of Gem TD. Warcraft 3 & Starcraft 2 both have a gem TD mod
map, but you must own WC3 to play the map, and the other requires you to download 15GB of starcraft 2 content, just to 
play a single mod map in the arcade. (there is a flash version of GEM TD. but there are lots of things wrong with it)

It's about time GEM TD became a stand-alone game. The other versions of GEM TD were built inside map editors in already
made RTS engines. you had to have a builder place your towers, was just one of the downsides to this. Building my own 
game from scratch means we don't have the need for a builder to place towers etc. The downside is building everything 
from scratch takes a lot of time.

EVERY texture and model in the game is subject to change, most are just placeholder models so I can debug and test 
things. the UI for now is very simple, also I needed to make sure things are working as planned, so there may be even 
more useless info put up in the future.For now, there is no damage test, no high score, and no end to the game. It's an endless maze tower Defence with 50 lives to give per play. (not per wave). 

Any and ALL feedback is welcome! I have a big list of To-Dos & bugs myself, but when others report bugs or errors, I get 
to add priority to that issue. With input & feedback from you! we can make a stand-alone Gem TD a reality!

There will be lots of updates to this game, and I will be putting out test builds as often as I can. Please follow us on
twitter & facebook for updates! and a good place to leave feedback!

https://twitter.com/RimrockGames

https://www.facebook.com/RimrockGames


Thank you! 
Brian Acker
-Rimrock Games

