Welcome to my Gem Tower Defense  Standalone PC game!  Gem TD was originally a mod td map for Warcraft 3. it was fairly popular. It has been remade (although not very well) in starcraft 2, flash, and windows phone. nobody can track down the original creator. 

This project is still in very early alpha. I have place holder art, and UI. I am actually almost done adding features for different towers, etc. there are however some big changes needed in the future such as Game Modes, difficulty settings, etc. I am working to replicate a fairly balanced fully functional gem td game before I add more.

Instructions!!
Every round you place 5 towers, they are random! you pick ONE of the 5 random towers you placed. and that tower is kept. the other 4 turn to maze rocks. you can place towers everywhere on the map except the waypoints, (Making a long MAZE between the waypoints is your best bet!)

if you place two of the same gems (type and size) during build mode, you can combine them into the a single gem of the next size up and same type.! Also you can downgrade any gem to the gem one size smaller but of the same type. 

Once you select a tower the next wave of creeps starts! they must touch every waypoint, in order, and then exit, in order to take a life. EVERY FOUR ROUNDS IS AIR ROUND, air creeps fly the path! AMETHYST gems can only attack air units... don't select one for your first gem!! 

There are special towers you can save gems for and select during the game! if you get all the parts to a special tower during ONE build mode, you can select that special tower on any of its parts that round. You can also save each part over a few rounds, and combine them into a special tower at any point. (the other parts will turn to maze rocks!)



slates are from GEM TD :D they can be placed on towers etc, but dont block creep path... they will be able to slow/stun/poison creeps that walk over them etc...